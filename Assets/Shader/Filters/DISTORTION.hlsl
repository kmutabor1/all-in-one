#ifndef DISTORTION_INCLUDED
#define DISTORTION_INCLUDED


void Distort_float(float2 uv, float radius, out float2 Out)
{
	float theta = atan2(uv.y, uv.x);
	float r = pow(radius, _BarrelPower);
	uv.x = r * cos(theta);
	uv.y = r * sin(theta);

	Out = float2(0.5 * (uv + 1.0));
}

#endif // DISTORTION_INCLUDED
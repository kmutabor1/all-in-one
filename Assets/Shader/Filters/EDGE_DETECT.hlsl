#ifndef EDGEDETECT_INCLUDED
#define EDGEDETECT_INCLUDED

float getLum(float2 uv, float2 offset, float m)
{
	float3 col = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, uv + offset);
	float lum = col.r * 0.3 + col.g * 0.59 + col.b * 0.11;
	return lum * m;
}

void Sobel_half(float2 uv, out float3 Out)
{
	float x = 0;
	float y = 0;
	float2 texelSize = _MainTex_TexelSize;

	x += getLum(uv, float2(-texelSize.x, -texelSize.y), -1.0);
	x += getLum(uv, float2(-texelSize.x, 0), -2.0);
	x += getLum(uv, float2(-texelSize.x, texelSize.y), -1.0);

	x += getLum(uv, float2(texelSize.x, -texelSize.y), 1.0);
	x += getLum(uv, float2(texelSize.x, 0), 2.0);
	x += getLum(uv, float2(texelSize.x, texelSize.y), 1.0);

	y += getLum(uv, float2(-texelSize.x, -texelSize.y), -1.0);
	y += getLum(uv, float2(0, -texelSize.y), -2.0);
	y += getLum(uv, float2(texelSize.x, -texelSize.y), -1.0);

	y += getLum(uv, float2(-texelSize.x, texelSize.y), 1.0);
	y += getLum(uv, float2(0, texelSize.y), 2.0);
	y += getLum(uv, float2(texelSize.x, texelSize.y), 1.0);


	Out = sqrt(x * x + y * y);
}


#endif // EDGEDETECT_INCLUDED
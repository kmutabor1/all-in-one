#ifndef CALCULATEREGION_INCLUDED
#define CALCULATEREGION_INCLUDED


void CalculateRegion_float(int2 lower, int2 upper, int samples, float2 uv, out float3 mean, out float variance)
{
	float3 sum = 0.0;
	float3 squareSum = 0.0;

	for (int x = lower.x; x <= upper.x; ++x)
	{
		for (int y = lower.y; y <= upper.y; ++y)
		{
			float2 offset = float2(_MainTex_TexelSize.x * x, _MainTex_TexelSize.y * y);
			float3 col = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, uv + offset);

			sum += col;
			squareSum += col * col;
		}
	}

	mean = sum / samples;

	variance = length(abs((squareSum / samples) - (mean * mean)));
	
}


#endif // CALCULATEREGION_INCLUDED
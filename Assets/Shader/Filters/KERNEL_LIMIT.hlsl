#ifndef KERNELLIMIT_INCLUDED
#define KERNELLIMIT_INCLUDED

float Gaussian(int x, int y, float _Spread)
{
	float sigmaSqu = (_Spread * _Spread);
	return (1 / sqrt(6.28319 * sigmaSqu)) * pow(2.71828, -((x * x) + (y * y)) / (2 * sigmaSqu));
}

void KernelLimits_half (float _Kernel, float _Spread, float alpha, float2 UV, out float4 Out)
{
	float4 col = float4(1,1,1,1);
	float kernelSum = 0.0;

	int upper = ((_Kernel - 1) / 2);
	int lower = -upper;

	for (int x = lower; x <= upper; ++x)
	{
		for (int y = lower; y <= upper; ++y)
		{
			float gauss = Gaussian(x, y, _Spread);
			kernelSum += gauss;
			float2 offset = float2(_MainTex_TexelSize.x * x, _MainTex_TexelSize.y * y);
			col += gauss * SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, UV + offset);
		}
	}
	col /= kernelSum;

	Out = float4(col.x,col.y,col.z, alpha);
}

#endif // KERNELLIMIT_INCLUDED
// GENERATED AUTOMATICALLY FROM 'Assets/Controls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Controls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Controls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Controls"",
    ""maps"": [
        {
            ""name"": ""Camera"",
            ""id"": ""fd8e755f-3834-47b7-b6b9-6aa2ae7e1c92"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""PassThrough"",
                    ""id"": ""864f9060-6945-415c-9ae7-1f65956105ae"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Look"",
                    ""type"": ""PassThrough"",
                    ""id"": ""0d21104f-0457-45ea-998d-3cb3a61fe9a7"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MoveVertical"",
                    ""type"": ""PassThrough"",
                    ""id"": ""ba7aa0d3-cc6c-4f44-baaa-2d259a83301c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Filter"",
                    ""type"": ""Button"",
                    ""id"": ""6259e5ae-9917-4fcd-a2a3-ec3f5ae50588"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""DragDrop"",
                    ""type"": ""PassThrough"",
                    ""id"": ""0ec95a70-45ee-4968-b03e-7fc64069a6f8"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Delete"",
                    ""type"": ""PassThrough"",
                    ""id"": ""4a65f7f2-45ef-4d1e-a68f-a1ebea399911"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""0e4c176d-5ac6-4870-9f99-963f88ff8cf5"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""0538c70f-36a7-41ed-8d66-38b377f4e409"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""7249e1db-86d7-425b-9750-fdd6a373db83"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""658a08cb-9472-43e1-9c30-05a4341849d0"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""d344c5c4-e750-4c09-9f52-45dbd6f11294"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""44058251-a737-47e8-a527-d796e13b3c01"",
                    ""path"": ""<Pointer>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""cd5e6206-bd8e-4bdf-91d2-e1598a2186f7"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveVertical"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""cfe9bd58-f07f-4f5d-9d47-982c09d80ce1"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveVertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""efcc1570-3ba0-4eb9-81ac-edd1eead9fb6"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveVertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""34054380-4093-4665-b151-dd59f78aedb7"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Filter"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f9686a1c-c0d6-448d-9783-176aa8e51e4e"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": ""Hold(pressPoint=0.1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DragDrop"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2e5842b5-1803-4dc6-a962-8afd7a60e13d"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Delete"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Camera
        m_Camera = asset.FindActionMap("Camera", throwIfNotFound: true);
        m_Camera_Move = m_Camera.FindAction("Move", throwIfNotFound: true);
        m_Camera_Look = m_Camera.FindAction("Look", throwIfNotFound: true);
        m_Camera_MoveVertical = m_Camera.FindAction("MoveVertical", throwIfNotFound: true);
        m_Camera_Filter = m_Camera.FindAction("Filter", throwIfNotFound: true);
        m_Camera_DragDrop = m_Camera.FindAction("DragDrop", throwIfNotFound: true);
        m_Camera_Delete = m_Camera.FindAction("Delete", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Camera
    private readonly InputActionMap m_Camera;
    private ICameraActions m_CameraActionsCallbackInterface;
    private readonly InputAction m_Camera_Move;
    private readonly InputAction m_Camera_Look;
    private readonly InputAction m_Camera_MoveVertical;
    private readonly InputAction m_Camera_Filter;
    private readonly InputAction m_Camera_DragDrop;
    private readonly InputAction m_Camera_Delete;
    public struct CameraActions
    {
        private @Controls m_Wrapper;
        public CameraActions(@Controls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_Camera_Move;
        public InputAction @Look => m_Wrapper.m_Camera_Look;
        public InputAction @MoveVertical => m_Wrapper.m_Camera_MoveVertical;
        public InputAction @Filter => m_Wrapper.m_Camera_Filter;
        public InputAction @DragDrop => m_Wrapper.m_Camera_DragDrop;
        public InputAction @Delete => m_Wrapper.m_Camera_Delete;
        public InputActionMap Get() { return m_Wrapper.m_Camera; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(CameraActions set) { return set.Get(); }
        public void SetCallbacks(ICameraActions instance)
        {
            if (m_Wrapper.m_CameraActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_CameraActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_CameraActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_CameraActionsCallbackInterface.OnMove;
                @Look.started -= m_Wrapper.m_CameraActionsCallbackInterface.OnLook;
                @Look.performed -= m_Wrapper.m_CameraActionsCallbackInterface.OnLook;
                @Look.canceled -= m_Wrapper.m_CameraActionsCallbackInterface.OnLook;
                @MoveVertical.started -= m_Wrapper.m_CameraActionsCallbackInterface.OnMoveVertical;
                @MoveVertical.performed -= m_Wrapper.m_CameraActionsCallbackInterface.OnMoveVertical;
                @MoveVertical.canceled -= m_Wrapper.m_CameraActionsCallbackInterface.OnMoveVertical;
                @Filter.started -= m_Wrapper.m_CameraActionsCallbackInterface.OnFilter;
                @Filter.performed -= m_Wrapper.m_CameraActionsCallbackInterface.OnFilter;
                @Filter.canceled -= m_Wrapper.m_CameraActionsCallbackInterface.OnFilter;
                @DragDrop.started -= m_Wrapper.m_CameraActionsCallbackInterface.OnDragDrop;
                @DragDrop.performed -= m_Wrapper.m_CameraActionsCallbackInterface.OnDragDrop;
                @DragDrop.canceled -= m_Wrapper.m_CameraActionsCallbackInterface.OnDragDrop;
                @Delete.started -= m_Wrapper.m_CameraActionsCallbackInterface.OnDelete;
                @Delete.performed -= m_Wrapper.m_CameraActionsCallbackInterface.OnDelete;
                @Delete.canceled -= m_Wrapper.m_CameraActionsCallbackInterface.OnDelete;
            }
            m_Wrapper.m_CameraActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Look.started += instance.OnLook;
                @Look.performed += instance.OnLook;
                @Look.canceled += instance.OnLook;
                @MoveVertical.started += instance.OnMoveVertical;
                @MoveVertical.performed += instance.OnMoveVertical;
                @MoveVertical.canceled += instance.OnMoveVertical;
                @Filter.started += instance.OnFilter;
                @Filter.performed += instance.OnFilter;
                @Filter.canceled += instance.OnFilter;
                @DragDrop.started += instance.OnDragDrop;
                @DragDrop.performed += instance.OnDragDrop;
                @DragDrop.canceled += instance.OnDragDrop;
                @Delete.started += instance.OnDelete;
                @Delete.performed += instance.OnDelete;
                @Delete.canceled += instance.OnDelete;
            }
        }
    }
    public CameraActions @Camera => new CameraActions(this);
    public interface ICameraActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnLook(InputAction.CallbackContext context);
        void OnMoveVertical(InputAction.CallbackContext context);
        void OnFilter(InputAction.CallbackContext context);
        void OnDragDrop(InputAction.CallbackContext context);
        void OnDelete(InputAction.CallbackContext context);
    }
}

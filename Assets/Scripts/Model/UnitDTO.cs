using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UnitDTO
{
    public string PrefabName { get; set; }
    public float[] Position { get; set; }
    public float[] Rotation { get; set; }

}
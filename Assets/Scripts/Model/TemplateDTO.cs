using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TemplateDTO
{
    public List<UnitDTO> units { get; set; }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.FlagsAttribute]
public enum WallStatesEnum : byte
{
    // 0000 -> NO WALLS
    // 1111 -> LEFT,RIGHT,UP,DOWN
    LEFT = 1, // 0001
    RIGHT = 2, // 0010
    UP = 4, // 0100
    DOWN = 8, // 1000

    VISITED = 128 // 1000 0000
}

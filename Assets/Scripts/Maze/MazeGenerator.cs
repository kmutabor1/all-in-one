using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MazeGenerator
{

    int width;
    int height;
    int deep;

    private System.Random rng;

    public MazeGenerator()
    {
        this.width = 2;
        this.height = 2;
    }

    public MazeGenerator(int width, int height)
    {
        this.width = width;
        this.height = height;
        
    }

    public MazeGenerator(int width, int height, int deep)
    {
        this.width = width;
        this.height = height;
        if(deep < 1)
        {
            this.deep = 1;
        } else
        {
            this.deep = deep;
        }
        
    }


    public void setDimension(int width, int height)
    {
        this.width = width;
        this.height = height;
    }
    
    public void setRNG(System.Random randomizer)
    {
        rng = randomizer;
    }


    public List<WallStatesEnum[,]> GenerateMazes()
    {
        List<WallStatesEnum[,]> mazes = new List<WallStatesEnum[,]>(deep);
        
        for (int k = 0; k < deep; ++k)
        {
            mazes.Add(GenerateMaze());
        }

        return mazes;

    }

    public WallStatesEnum[,] GenerateMaze()
    {

        WallStatesEnum[,] maze = new WallStatesEnum[width, height];

        WallStatesEnum initial = WallStatesEnum.RIGHT | WallStatesEnum.LEFT | WallStatesEnum.UP | WallStatesEnum.DOWN;

        for (int i = 0; i < width; ++i)
        {
            for (int j = 0; j < height; ++j)
            {
                maze[i, j] = initial;  // 1111
            }
        }

        return ApplyRecursiveBacktracker(maze);

    }


    private WallStatesEnum[,] ApplyRecursiveBacktracker(WallStatesEnum[,] maze)
    {
        // here we make changes
        var positionStack = new Stack<PositionStruct>();
        var position = new PositionStruct { X = rng.Next(0, width), Y = rng.Next(0, height) };

        maze[position.X, position.Y] |= WallStatesEnum.VISITED;  // 1000 1111
        positionStack.Push(position);

        while (positionStack.Count > 0)
        {
            var current = positionStack.Pop();
            var neighbours = GetUnvisitedNeighbours(current, maze);

            if (neighbours.Count > 0)
            {
                positionStack.Push(current);

                var randIndex = rng.Next(0, neighbours.Count);
                var randomNeighbour = neighbours[randIndex];

                var nPosition = randomNeighbour.Position;
                maze[current.X, current.Y] &= ~randomNeighbour.SharedWall;
                maze[nPosition.X, nPosition.Y] &= ~GetOppositeWall(randomNeighbour.SharedWall);
                maze[nPosition.X, nPosition.Y] |= WallStatesEnum.VISITED;

                positionStack.Push(nPosition);
            }
        }

        return maze;
    }




    private List<NeighbourStruct> GetUnvisitedNeighbours(PositionStruct p, WallStatesEnum[,] maze)
    {
        var list = new List<NeighbourStruct>();

        if (p.X > 0) // left
        {
            if (!maze[p.X - 1, p.Y].HasFlag(WallStatesEnum.VISITED))
            {
                list.Add(new NeighbourStruct
                {
                    Position = new PositionStruct
                    {
                        X = p.X - 1,
                        Y = p.Y
                    },
                    SharedWall = WallStatesEnum.LEFT
                });
            }
        }

        if (p.Y > 0) // DOWN
        {
            if (!maze[p.X, p.Y - 1].HasFlag(WallStatesEnum.VISITED))
            {
                list.Add(new NeighbourStruct
                {
                    Position = new PositionStruct
                    {
                        X = p.X,
                        Y = p.Y - 1
                    },
                    SharedWall = WallStatesEnum.DOWN
                });
            }
        }

        if (p.Y < height - 1) // UP
        {
            if (!maze[p.X, p.Y + 1].HasFlag(WallStatesEnum.VISITED))
            {
                list.Add(new NeighbourStruct
                {
                    Position = new PositionStruct
                    {
                        X = p.X,
                        Y = p.Y + 1
                    },
                    SharedWall = WallStatesEnum.UP
                });
            }
        }

        if (p.X < width - 1) // RIGHT
        {
            if (!maze[p.X + 1, p.Y].HasFlag(WallStatesEnum.VISITED))
            {
                list.Add(new NeighbourStruct
                {
                    Position = new PositionStruct
                    {
                        X = p.X + 1,
                        Y = p.Y
                    },
                    SharedWall = WallStatesEnum.RIGHT
                });
            }
        }

        return list;
    }



    private WallStatesEnum GetOppositeWall(WallStatesEnum wall)
    {
        switch (wall)
        {
            case WallStatesEnum.RIGHT: return WallStatesEnum.LEFT;
            case WallStatesEnum.LEFT: return WallStatesEnum.RIGHT;
            case WallStatesEnum.UP: return WallStatesEnum.DOWN;
            case WallStatesEnum.DOWN: return WallStatesEnum.UP;
            default: return WallStatesEnum.LEFT;
        }
    }

}

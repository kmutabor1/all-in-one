using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct PositionStruct 
{
    public int X;
    public int Y;
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct NeighbourStruct 
{
    public PositionStruct Position;
    public WallStatesEnum SharedWall;
}

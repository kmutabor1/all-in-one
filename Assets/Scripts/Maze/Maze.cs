using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Maze : MonoBehaviour
{

    private readonly System.Random rng = new System.Random(/*seed*/);

    [SerializeField]
    [Min(2)]
    private int width = 2;
    [SerializeField]
    [Min(2)]
    private int height = 2;
  
    private MazeGenerator mazeGenerator = new MazeGenerator();

    [SerializeField]
    private Transform unitPrefab = null;


    public delegate void OnRegenerate();
    public static event OnRegenerate OnRegenerateEvent;
    public static void RaiseOnRegenerate()
    {
        if (OnRegenerateEvent != null)
        {
            OnRegenerateEvent();                       //Invoke an Event
        }
    }

    private void OnEnable()
    {
        OnRegenerateEvent += DrawLevel;
    }


    void Start()
    {

        mazeGenerator.setRNG(rng);

        //DrawLevel();

    }

    
    public void increaseWidth()
    {
        this.width++;
        setDimendion(width, this.height);
    }

    public void increaseHeight()
    {
        this.height++;
        setDimendion(this.width, height);
    }


    public void ChangeDifficulty()
    {
        if (this.width > this.height)
        {
            increaseHeight();
        }
        else if (this.width <= this.height)
        {
            increaseWidth();
        }
    }

    public void setDimendion(int width, int height)
    {
        this.width = width;
        this.height = height;

        mazeGenerator.setDimension(this.width, this.height);
    }

    

    public void DrawLevel()
    {

        RemoveObjectsByTag("MazeUnit");

        setDimendion(width, height);        

        WallStatesEnum[,] maze = mazeGenerator.GenerateMaze();
        int exitWidth = rng.Next(0, width);
        int exitHeight = rng.Next(0, height);
        //Vector3 exitPosition = evalMazeElementPosition(exitWidth, exitHeight);

        List<KeyValuePair<string, Vector3>> rowsVertical = new List<KeyValuePair<string, Vector3>>();
        List<KeyValuePair<string, Vector3>> rowsHorizontal = new List<KeyValuePair<string, Vector3>>();

        // Instantiate level prefabs
        for (int i = 0; i < width; ++i)
        {
            for (int j = 0; j < height; ++j)
            {
                var cell = maze[i, j];

                //Debug.Log("Cell (" + i + " " + j + "): " + cell);

                //var position = new Vector3(-width / 2 + i, 0, -height / 2 + j);
                Vector3 position = evalMazeElementPosition(i, j);

                if (cell.HasFlag(WallStatesEnum.UP))
                {
                    if (j < height - 1) {
                        rowsHorizontal.Add(new KeyValuePair<string, Vector3>("Wall_T_" + j + "_" + i, position));
                    }
                }

                if (cell.HasFlag(WallStatesEnum.LEFT))
                {
                    rowsVertical.Add(new KeyValuePair<string, Vector3>("Wall_L_" + i + "_" + j, position));
                }


                if (i != exitWidth || j != exitHeight)
                {
                    Transform floor = Instantiate(unitPrefab, transform);
                    floor.eulerAngles = new Vector3(90, 0, 0);
                    Vector3 pos = evalMazeElementPosition(i, j) + new Vector3(0, -0.5f, 0f);
                    floor.position = pos;
                }

            }
        }


        for (int i = 1; i < width; ++i)
        {
            Predicate<KeyValuePair<string, Vector3>> p = (s) => { return s.Key.StartsWith("Wall_L_" + i + "_"); };
            List<KeyValuePair<string, Vector3>> l = rowsVertical.FindAll(p);
            //Debug.Log("Wall L at >>> " + i + " = " + l.Count);

 
            List<KeyValuePair<string, Vector3>>.Enumerator enumerator = l.GetEnumerator();
            int start, end;
            while (enumerator.MoveNext())
            {
                KeyValuePair<string, Vector3> s = enumerator.Current;
                start = int.Parse(s.Key.Substring(s.Key.LastIndexOf("_") + 1));
                end = start + 1;

                KeyValuePair<string, Vector3> nextWallData = l.Find((s) => {
                    return s.Key.Equals("Wall_L_" + i + "_" + (end));
                });

                while (nextWallData.Key != null && !nextWallData.Key.Equals(""))
                {
                    enumerator.MoveNext();
                    end++;
                    nextWallData = l.Find((s) => {
                        return s.Key.Equals("Wall_L_" + i + "_" + end);
                    });

                }
                
                //Debug.Log("Start: " + start + ", End: " + end);

                Transform wall = Instantiate(unitPrefab, transform);
                wall.name = s.Key;
                int length = end - start;
                //wall.position = s.Value + new Vector3(-1f / 2, 0, (length % 2 == 0 ? (length / 2) - 0.5f : (length / 2)));
                wall.localScale = new Vector3(length, wall.localScale.y, wall.localScale.z);
                wall.eulerAngles = new Vector3(0, 90, 0);
                
                Vector3 pos = s.Value + new Vector3(-1f / 2, 0, (length % 2 == 0 ? (length / 2) - 0.5f : (length / 2)));
                wall.position = pos;
            }

        }
        rowsVertical.Clear();

   

        for (int j = 0; j < height; ++j)
        {
            Predicate<KeyValuePair<string, Vector3>> p = (s) => { return s.Key.StartsWith("Wall_T_" + j + "_"); };
            List<KeyValuePair<string, Vector3>> l = rowsHorizontal.FindAll(p);
            
            List<KeyValuePair<string, Vector3>>.Enumerator enumerator = l.GetEnumerator();
            int start, end;
            while (enumerator.MoveNext())
            {
                KeyValuePair<string, Vector3> s = enumerator.Current;
                start = int.Parse(s.Key.Substring(s.Key.LastIndexOf("_") + 1));
                end = start + 1;

                KeyValuePair<string, Vector3> nextWallData = l.Find((s) => {
                    return s.Key.Equals("Wall_T_" + j + "_" + (end));
                });

                while (nextWallData.Key != null && !nextWallData.Key.Equals(""))
                {
                    enumerator.MoveNext();
                    end++;
                    nextWallData = l.Find((s) => {
                        return s.Key.Equals("Wall_T_" + j + "_" + end);
                    });

                }

                //Debug.Log("Start: " + start + ", End: " + end);

                Transform wall = Instantiate(unitPrefab, transform);
                wall.name = s.Key;
                int length = end - start;
                //wall.position = s.Value + new Vector3((length % 2 == 0 ? (length / 2) - 0.5f : (length / 2)), 0, 1f / 2);
                wall.localScale = new Vector3(length, wall.localScale.y, wall.localScale.z);

                Vector3 pos = s.Value + new Vector3((length % 2 == 0 ? (length / 2) - 0.5f : (length / 2)), 0, 1f / 2);
                wall.position = pos;
            }
        }

        rowsHorizontal.Clear();

        DestructableController.RaiseOnObjectReady();

    }


    private Vector3 evalMazeElementPosition(int i, int j) { 
        return new Vector3(-width / 2 + i, 0, -height / 2 + j);
    }



    private void GenerateFloor()
    {
        int exitWidth = rng.Next(0, width);
        int exitHeight = rng.Next(0, height);

        //Vector3 exitPosition = evalMazeElementPosition(exitWidth, exitHeight);

        for (int i = 0; i < width; ++i)
        {
            for (int j = 0; j < height; ++j)
            {
                if (i == exitWidth && j == exitHeight) {
                    continue;
                }

                Transform floor  = Instantiate(unitPrefab, transform);
                floor.eulerAngles = new Vector3(90, 0, 0);
                Vector3 pos = evalMazeElementPosition(i, j) + new Vector3(0, -0.5f, 0f);
                floor.position = pos;
            }
        }


                //int nW = Mathf.RoundToInt(width / 2);
                //int nWv = rng.Next(-nW, nW);

                //int nH = Mathf.RoundToInt(height / 2);
                //int nHv = rng.Next(-nH, nH);

                //Vector3 startDrawPosition = new Vector3(nWv, transform.localPosition.y + 0.5f, nHv);



            }

    public void ResetPosition() {
        transform.rotation = Quaternion.identity;
        transform.Rotate(Vector3.up, 180f);
    }




    public void RemoveObjectsByTag(string tag)
    {
        foreach (GameObject o in GameObject.FindGameObjectsWithTag(tag))
        {
            Destroy(o);
        }
    }


   

    
}

using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class UnitController : MonoBehaviour
{
    
    private Vector3 _targetPosition;
    private Quaternion _targetRotation;
    private Vector3 _targetScale;
    private float _restoreDuration;
    private float _scaleUpDuration;
    private float _explosionMaxDistance;

    private Rigidbody _rigidbody;
    private Vector3 _forceDirection;
    private Vector3 _forceTorque;
    private float _forceSpeed;
    private float _torqueSpeed;
    private bool isCanExplosion = false;
    private Vector3 _rootPosition;
   
    public delegate void OnExplosion();
    public static event OnExplosion OnExplosionEvent;
    public static void RaiseOnExplosion()
    {
        if (OnExplosionEvent != null)
        {
            OnExplosionEvent();                       //Invoke an Event
        }
    }


    private void EnableExplosion()
    {
        if (_rigidbody != null) { _rigidbody.isKinematic = false; };

        if (!isCanExplosion) {
            if (this != null) {
                BeginScale(_scaleUpDuration);
            }
        }

        isCanExplosion = true;

        
    }

    private void OnEnable()
    {
        OnExplosionEvent += EnableExplosion;
    }

    private void Start()
    {
        _rootPosition = transform.position;
        _rigidbody = transform.GetComponent<Rigidbody>();
        _rigidbody.useGravity = false;
        _rigidbody.isKinematic = false;
        _forceDirection = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        _forceTorque = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
    }

    private void FixedUpdate()
    {
        if(isCanExplosion)
        {
            _rigidbody.AddForce(_forceDirection * _forceSpeed * Time.fixedDeltaTime, ForceMode.Acceleration);
            _rigidbody.AddRelativeTorque(_forceTorque * _torqueSpeed * Time.fixedDeltaTime, ForceMode.Acceleration);

            if (Vector3.Distance(_rootPosition, transform.position) > _explosionMaxDistance) {
                isCanExplosion = false;
                BeginTranslation(_restoreDuration);
            }
        }
        
    }


    private void BeginScale(float duration) {
        StartCoroutine(DoScale(_targetScale, duration));
    }

    private void BeginTranslation(float duration) { 
        StartCoroutine(DoTransform(_targetPosition, _targetRotation, duration)); 
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }



    public void SetTargets(Vector3 position, Quaternion rotation, Vector3 scale, float restoreDuration, float scaleUpDuration, float explosionMaxDistance, float forceSpeed, float torqueSpeed) {
        _targetPosition = position;
        _targetRotation = rotation;
        _targetScale = scale;
        _restoreDuration = restoreDuration;
        _scaleUpDuration = scaleUpDuration;
        _explosionMaxDistance = explosionMaxDistance;
        _forceSpeed = forceSpeed;
        _torqueSpeed = torqueSpeed;
    }


    private IEnumerator DoTransform(Vector3 targetPosition, Quaternion targetRotation, float duration)
    {
        
        float time = 0;
        Vector3 startPosition = transform.localPosition;
        Quaternion startRotation = transform.localRotation;
        
        float distance;

        while (time < duration)
        {
            distance = time / duration;
            transform.localPosition = Vector3.Lerp(startPosition, targetPosition, distance);
            transform.localRotation = Quaternion.Lerp(startRotation, targetRotation, distance);
            time += Time.deltaTime;
            yield return null;
        }
        _rigidbody.isKinematic = true;
        transform.localPosition = targetPosition;
        transform.localRotation = targetRotation;
    }

    private IEnumerator DoScale(Vector3 targetScale, float duration)
    {
        float time = 0;
        Vector3 startScale = transform.localScale;

        float distance;

        while (time < duration)
        {
            distance = time / duration;
            transform.localScale = Vector3.Lerp(startScale, targetScale, distance);
            time += Time.deltaTime;
            yield return null;
        }
        transform.localScale = targetScale;

    }


}

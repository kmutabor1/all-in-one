using System.Collections.Generic;
using UnityEditor.ProBuilder;
using UnityEngine;
using UnityEngine.ProBuilder;
using UnityEngine.ProBuilder.MeshOperations;
 
public class DestructableRectangle : MonoBehaviour
{
    [SerializeField]
    private Primitives _primitive = Primitives.Cube;
    [SerializeField]
    private Material _material;
    [SerializeField]
    private PhysicMaterial _physicMaterial;
    [SerializeField]
    [Range(0f, 1f)]
    private float _subElementPercentSize = 0.1f;

    [Header("Physics")]
    [SerializeField]
    private bool _addRigidBody = false;
    [SerializeField]
    [Min(0)]
    private float _subElementMass = 0.01f;
    [SerializeField]
    [Min(0)]
    private float _drag = 0f;
    [SerializeField]
    [Min(0)]
    private float _angularDrag = 0.05f;
    [SerializeField]
    private bool _useGravity = false;
    [SerializeField]
    private bool _isKinematic = false;
    [SerializeField]
    private bool _addGravityController = true;

    private PrimitiveType _primitiveType;


    private void Awake()
    {
        switch (_primitive)
        {
            case Primitives.Cube: _primitiveType = PrimitiveType.Cube; break;
            case Primitives.Sphere: _primitiveType = PrimitiveType.Sphere; break;
        }

    }

    

    void Start()
    {
        CreateDestructableRectangle();
    }


    private void OnDrawGizmos()
    {
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
        Gizmos.color = new Color(1f, 1f, 1f, 0.05f);
        Gizmos.DrawCube(Vector3.zero, Vector3.one);
    }


    private void CreateDestructableRectangle() {

        Quaternion localRotation = transform.rotation;
        transform.rotation = Quaternion.identity;

        float primitiveSize = Mathf.Min(new float[] { transform.lossyScale.x, transform.lossyScale.y, transform.lossyScale.z });
        float elementScale = primitiveSize * _subElementPercentSize;
        int xAmount = Mathf.RoundToInt(transform.lossyScale.x / elementScale);
        int yAmount = Mathf.RoundToInt(transform.lossyScale.y / elementScale);
        int zAmount = Mathf.RoundToInt(transform.lossyScale.z / elementScale);

        Debug.Log("one element scale: " + elementScale);
        Debug.Log(">>> x: " + xAmount + " >>> y: " + yAmount + " >>> z: " + zAmount);

        float halfElementScale = elementScale / 2;

        float startX = -(transform.lossyScale.x * 0.5f) + halfElementScale;
        float startY = -(transform.lossyScale.y * 0.5f) + halfElementScale;
        float startZ = -(transform.lossyScale.z * 0.5f) + halfElementScale;

        Transform rootTransform = Instantiate(
                new GameObject("Destructabletransform block"), 
                transform.position, 
                transform.rotation, 
                transform.parent
            ).transform;

        for (int x = 0; x < xAmount; x++)
        {
            for (int y = 0; y < yAmount; y++)
            {
                for (int z = 0; z < zAmount; z++)
                {
                    GameObject primitive = GameObject.CreatePrimitive(_primitiveType);
                    Renderer primitiveRenderer = primitive.GetComponent<Renderer>();
                    primitiveRenderer.sharedMaterial = _material;
                    primitive.transform.parent = rootTransform;
                    primitive.transform.position = transform.localPosition +
                        new Vector3(
                            startX + (x * elementScale),
                            startY + (y * elementScale),
                            startZ + (z * elementScale)
                        );
                    primitive.transform.localScale *= elementScale;

                    if (_addRigidBody)
                    {
                        Rigidbody rigidbody = primitive.AddComponent<Rigidbody>();
                        rigidbody.interpolation = RigidbodyInterpolation.Interpolate;
                        rigidbody.collisionDetectionMode = CollisionDetectionMode.Continuous;
                        rigidbody.mass = _subElementMass;
                        rigidbody.drag = _drag;
                        rigidbody.angularDrag = _angularDrag;
                        rigidbody.useGravity = _useGravity;
                        rigidbody.isKinematic = _isKinematic;


                        if (_addGravityController) {
                            primitive.AddComponent<GravityController>();
                            Collider collider = primitive.GetComponent<Collider>();
                            collider.material = _physicMaterial;
                        }
                        
                    }
                }
            }
        }

        rootTransform.rotation = localRotation;
        rootTransform.localPosition = transform.position;
        transform.rotation = localRotation;

        Destroy(gameObject);
        
    }

    private enum Primitives { 
        Cube, Sphere
    }

}

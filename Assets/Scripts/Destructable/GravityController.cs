using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class GravityController : MonoBehaviour
{
    private Rigidbody _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }


    private void ToggleGravity() {
        _rigidbody.useGravity = !_rigidbody.useGravity;
    }

   

    private delegate void OnGavityToggle();
    private static event OnGavityToggle OnGavityToggleEvent;
    public static void RaiseOnGavity() { 
        if (OnGavityToggleEvent != null) {
            OnGavityToggleEvent();                    
        }
    }

    private void OnEnable()
    {
        OnGavityToggleEvent += ToggleGravity;
    }

    private void OnDisable()
    {
        OnGavityToggleEvent -= ToggleGravity;
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class ReplaceWithBroken : MonoBehaviour
{
    
    private bool _isBroken = false;

    private void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Player"))
        {
            if (!_isBroken)
            {
                _isBroken = true;
                Broke();
            }
        }

    }

    private void Broke() {
        GravityController.RaiseOnGavity();
    }


}

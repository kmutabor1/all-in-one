using System.Collections.Generic;
using UnityEditor.ProBuilder;
using UnityEngine;
using UnityEngine.ProBuilder;
using UnityEngine.ProBuilder.MeshOperations;


public class Destructable : MonoBehaviour
{

    [SerializeField]
    [Range(0f, 1f)]
    private float _subElementPercentSize = 0.1f;
    [SerializeField]
    private PrimitiveType _primitiveType = PrimitiveType.Cube;
    

    void Start()
    {
        MeshFilter meshFilter = GetComponent<MeshFilter>();
        Mesh mesh = meshFilter.sharedMesh;
        List<Vector3> uniquieVerticePositions = new List<Vector3>();
        Debug.Log("V AMOUNT: " + mesh.GetVertices().Length);
        foreach (Vertex v in mesh.GetVertices())
            {
            if (!uniquieVerticePositions.Contains(v.position)) {
                uniquieVerticePositions.Add(v.position);
            }
        }
        Debug.Log("P AMOUNT: " + uniquieVerticePositions.Count);

        float minSide = Mathf.Min(new float[] { transform.localScale.x, transform.localScale.y, transform.localScale.z });
        float elementScale = minSide * _subElementPercentSize;
        int xAmount = Mathf.CeilToInt(transform.localScale.x / elementScale); 
        int yAmount = Mathf.CeilToInt(transform.localScale.y / elementScale);
        int zAmount = Mathf.CeilToInt(transform.localScale.z / elementScale);
        Debug.Log("one element scale: " + elementScale);
        Debug.Log(">>> x: " + xAmount + " >>> y: " + yAmount + " >>> z: " + zAmount);

        
        foreach (Vector3 v in uniquieVerticePositions)
        {
            GameObject primitive;
            //= GameObject.CreatePrimitive(PrimitiveType.Cube);
            //primitive.transform.parent = transform;
            //primitive.transform.localScale *= _subElementPercentSize;
            //primitive.transform.localPosition = v;
            //Debug.Log(v);

            yAmount = Mathf.Abs(Mathf.CeilToInt(v.y / elementScale));
            //for (int x = 0; x < xAmount; x++)
            //{
                for (int y = 0; y < yAmount; y++)
                {
                    //for (int z = 0; z < zAmount; z++)
                    //{
                        primitive = GameObject.CreatePrimitive(_primitiveType);
                        primitive.transform.parent = transform;
                        primitive.transform.localScale *= _subElementPercentSize;
                        primitive.transform.position =
                            new Vector3(
                                v.x > 0 ? v.x - elementScale / 2 : v.x + elementScale / 2,
                                v.y > 0 ? v.y - (y * elementScale) - elementScale / 2 : v.y + (y * elementScale) + elementScale / 2,
                                v.z > 0 ? v.z - elementScale / 2 : v.z + elementScale / 2
                            );
                    //}
                }
            //}

        }


        /*
        float startX = -(transform.localScale.x * 0.5f);
        float startY = -(transform.localScale.y * 0.5f);
        float startZ = -(transform.localScale.z * 0.5f);

        for (int x = 0; x < xAmount; x++) 
        {
            for (int y = 0; y < yAmount; y++)
            {
                for (int z = 0; z < zAmount; z++)
                {
                    GameObject primitive = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    primitive.transform.parent = transform;
                    primitive.transform.localScale *= elementScale;
                    primitive.transform.position = 
                        new Vector3(
                            startX + (elementScale / 2) + (x * (elementScale)),
                            startY + (elementScale / 2) + (y * (elementScale)),
                            startZ + (elementScale / 2) + (z * (elementScale))
                        );
                }
            }
        }

        */



    }


}

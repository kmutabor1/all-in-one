using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class TemplatesManager : MonoBehaviour
{

    private const string PREFAB_MECH_1_NAME = "MECH_CYAN";
    private const string PREFAB_MECH_2_NAME = "MECH_METAL";
    private const string PREFAB_MECH_3_NAME = "MECH_YELLOW_GREEN";
    private const string PREFAB_MECH_4_NAME = "MECH_CONSTRUCT";
    private const string PREFAB_MECH_5_NAME = "MECH_PAINTBRUSH";


    [SerializeField]
    private Dropdown _TplDropDown;
    [SerializeField]
    private InputField _TplNameInput;


    [Header("Pawns block")]
    [SerializeField]
    private Transform[] _prefabs;
    [SerializeField]
    private Transform _parent;
    [SerializeField]
    private Material _dragMaterial;



    private Transform target;
    private Material targetBaseMat;

    private bool isCanDrag = false;

    private List<string> templateNames = new List<string>();
    private BinaryFormatter formatter = new BinaryFormatter();
    private const string TPL_SUFFIX = ".UnitTpl";


    private void Awake()
    {
        Controls _controls = ControlsC.getControls();
        _controls.Camera.DragDrop.started += cxt => { Select(); };
        _controls.Camera.DragDrop.canceled += cxt => { Drop(); };
        _controls.Camera.Delete.performed += cxt => { DeleteUnit(); };
    }


    void Start()
    {
        ReinitDropdown();
        LoadTemplate();

        Cursor.lockState = CursorLockMode.None;
    }


    private void FixedUpdate()
    {
        Drag();
    }

    public void SaveTemplate() {

        int amount = _parent.transform.childCount;

        TemplateDTO template = new TemplateDTO();
        List<UnitDTO> unitsDTO = new List<UnitDTO>(amount);
        for (int i = 0; i < amount; i++) {
            Transform t = _parent.transform.GetChild(i);

            UnitDTO unitDTO = new UnitDTO();
            unitDTO.PrefabName = t.GetComponent<Unit>().PrefabName;
            unitDTO.Position = new float[3] { t.localPosition.x, t.localPosition.y, t.localPosition.z };
            unitDTO.Rotation = new float[3] { t.localRotation.x, t.localRotation.y, t.localRotation.z };
            unitsDTO.Add(unitDTO);
            
        }

        if (unitsDTO.Count == 0) { return; }

        template.units = unitsDTO;

        string fileName = _TplNameInput.text.Trim();
        if(fileName.Equals(""))
        {
            DateTime now = DateTime.Now;
            fileName = now.Day + "-" + now.Month + "-" + now.Year + " " + now.Hour + "-" + now.Minute + "-" + now.Second;
        }

        using (FileStream stream = new FileStream(Application.streamingAssetsPath + "/" + fileName + TPL_SUFFIX, FileMode.Create))
        {
            formatter.Serialize(stream, template);
        }

        ReinitDropdown();

    }


    public void LoadTemplate() {
        if (_TplDropDown.options.Count < 1) { return; }

        TemplateDTO template = null;
        using (FileStream stream = new FileStream(Application.streamingAssetsPath + "/" + templateNames[_TplDropDown.value] + TPL_SUFFIX, FileMode.Open))
        {
            template = (TemplateDTO)formatter.Deserialize(stream);
        }

        if(template != null)
        {
            Unit[] units = _parent.GetComponentsInChildren<Unit>();

            //Debug.Log("LoadTemplate units lenght: " + units.Length);

            foreach(Unit u in units)
            {
                Destroy(u.gameObject);
            }


            foreach(UnitDTO unitDTO in template.units)
            {
                //Debug.Log("unitDTO.PrefabName = " + unitDTO.PrefabName);
                switch (unitDTO.PrefabName)
                {
                    case PREFAB_MECH_1_NAME: target = Instantiate(_prefabs[0], _parent); break;
                    case PREFAB_MECH_2_NAME: target = Instantiate(_prefabs[1], _parent); break;
                    case PREFAB_MECH_3_NAME: target = Instantiate(_prefabs[2], _parent); break;
                    case PREFAB_MECH_4_NAME: target = Instantiate(_prefabs[3], _parent); break;
                    case PREFAB_MECH_5_NAME: target = Instantiate(_prefabs[4], _parent); break;
                }

                target.localPosition = new Vector3(unitDTO.Position[0], unitDTO.Position[1], unitDTO.Position[2]);
                target.Rotate(unitDTO.Rotation[0], unitDTO.Rotation[1], unitDTO.Rotation[2]);

                Unit unit = target.GetComponent<Unit>();
                unit.PrefabName = unitDTO.PrefabName;
            }

            //container.BroadcastMessage("RestoreTransform", template.units, SendMessageOptions.DontRequireReceiver);

        }

    }



    public void SetNameInputField(int index) {
        _TplNameInput.text = templateNames[index];
    }

    public void DeleteTemplate()
    {
        if (_TplDropDown.options.Count < 1) { return; }

        string del = Application.streamingAssetsPath + "/" + templateNames[_TplDropDown.value] + TPL_SUFFIX;

        //print(del);

        File.Delete(del);
        _TplNameInput.text = "";
        ReinitDropdown();
    }



    private void ReinitDropdown() {
        DirectoryInfo directoryInfo = new DirectoryInfo(Application.streamingAssetsPath);
        //print("Streaming Assets Path: " + Application.streamingAssetsPath);
        FileInfo[] allFiles = directoryInfo.GetFiles("*.*");

        templateNames.Clear();
        _TplDropDown.ClearOptions();

        foreach (FileInfo file in allFiles)
        {
            if (file.Name.EndsWith(TPL_SUFFIX))
            {
                templateNames.Add(file.Name.Replace(TPL_SUFFIX, ""));
                //Debug.Log(file);
            }
        }

        _TplDropDown.AddOptions(templateNames);
        _TplDropDown.RefreshShownValue();

        _TplDropDown.value = 0;
        
        if(templateNames.Count > 0)
        {
            SetNameInputField(0);
        }

    }


    private void Select()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue()), out hit))
        {
            if (hit.transform.tag.Equals("Pawn"))
            {
                target = hit.transform;
                Rigidbody rb = target.gameObject.GetComponent<Rigidbody>();
                rb.isKinematic = true;
                target.position = new Vector3(target.position.x, hit.point.y + 2, target.position.z);
                Renderer renderer = target.gameObject.GetComponentInChildren<Renderer>();
                targetBaseMat = renderer.material;
                renderer.material = _dragMaterial;
                
                isCanDrag = true;
            }
        }
    }

    public void SelectCreate(string prefabName)
    {
        
        switch (prefabName)
        {
            case PREFAB_MECH_1_NAME: target = Instantiate(_prefabs[0], _parent); break;
            case PREFAB_MECH_2_NAME: target = Instantiate(_prefabs[1], _parent); break;
            case PREFAB_MECH_3_NAME: target = Instantiate(_prefabs[2], _parent); break;
            case PREFAB_MECH_4_NAME: target = Instantiate(_prefabs[3], _parent); break;
            case PREFAB_MECH_5_NAME: target = Instantiate(_prefabs[4], _parent); break;
            default: target = Instantiate(_prefabs[0], _parent); prefabName = PREFAB_MECH_1_NAME; break;
        }

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue()), out hit))
        {
            Rigidbody rb = target.gameObject.GetComponent<Rigidbody>();
            rb.isKinematic = true;
            target.position = new Vector3(hit.point.x, hit.point.y + 2, hit.point.z);
            Renderer renderer = target.gameObject.GetComponentInChildren<Renderer>();
            targetBaseMat = renderer.material;
            renderer.material = _dragMaterial;
        }

        Unit unit = target.GetComponent<Unit>();
        unit.PrefabName = prefabName;

        isCanDrag = true;

    }

    public void Drag()
    {
        if (isCanDrag)
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue()), out hit))
            {
                if (!hit.transform.tag.Equals("Pawn"))
                {
                    if (target != null)
                    {
                        target.position = new Vector3(hit.point.x, hit.point.y + 2, hit.point.z);
                    }
                }
            }
        }
    }

    public void Drop()
    {
        if (target != null)
        {
            Renderer renderer = target.gameObject.GetComponentInChildren<Renderer>();
            renderer.material = targetBaseMat;

            Rigidbody rb = target.gameObject.GetComponent<Rigidbody>();
            rb.isKinematic = false;

            target = null;
        }

        isCanDrag = false;
    }



    private void DeleteUnit()
    {
        if (isCanDrag)
        {
            if (target != null)
            {
                Destroy(target.gameObject);
                target = null;
            }
        }
    }
}





using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.ProBuilder;
using UnityEngine;
using UnityEngine.ProBuilder;
using UnityEngine.UIElements;

public class Generator : MonoBehaviour
{
    [SerializeField]
    private Transform _rootTransform;

    [SerializeField]
    private BaseShape _shapeType = BaseShape.Plane;

    [SerializeField]
    [ConditionalHide("_shapeType", new int[] { ((int)BaseShape.Stair), ((int)BaseShape.Door), ((int)BaseShape.Plane), ((int)BaseShape.Cube), ((int)BaseShape.Arch) })]
    private float _width;
    [SerializeField]
    [ConditionalHide("_shapeType", new int[] { ((int)BaseShape.Stair), ((int)BaseShape.Pipe), ((int)BaseShape.Door), ((int)BaseShape.Cylinder), ((int)BaseShape.Plane), ((int)BaseShape.Cube), ((int)BaseShape.Cone) })]
    private float _height;
    [SerializeField]
    [ConditionalHide("_shapeType", new int[] { ((int)BaseShape.Stair), ((int)BaseShape.Pipe), ((int)BaseShape.Door), ((int)BaseShape.Cube), ((int)BaseShape.Arch) })]
    private float _depth;
    [SerializeField]
    [ConditionalHide("_shapeType", new int[] { ((int)BaseShape.Pipe), ((int)BaseShape.Cylinder), ((int)BaseShape.Sphere), ((int)BaseShape.Arch), ((int)BaseShape.Cone) })]
    private float _radius;
    [SerializeField]
    [ConditionalHide("_shapeType", new int[] { ((int)BaseShape.Pipe), ((int)BaseShape.Cylinder), ((int)BaseShape.Sphere), ((int)BaseShape.Cone) })]
    private int _subdivision;
    [SerializeField]
    [ConditionalHide("_shapeType", new int[] { ((int)BaseShape.Arch) })]
    private float _angle;
    [SerializeField]
    [ConditionalHide("_shapeType", new int[] { ((int)BaseShape.Plane) })]
    private int _widthCuts;
    [SerializeField]
    [ConditionalHide("_shapeType", new int[] { ((int)BaseShape.Pipe), ((int)BaseShape.Cylinder), ((int)BaseShape.Plane) })]
    private int _heightCuts;
    [SerializeField]
    [ConditionalHide("_shapeType", new int[] { ((int)BaseShape.Plane) })]
    private Axis _axis = Axis.Up;
    [SerializeField]
    [ConditionalHide("_shapeType", new int[] { ((int)BaseShape.Arch) })]
    private int _radialCuts;
    [SerializeField]
    [ConditionalHide("_shapeType", new int[] { ((int)BaseShape.Arch) })]
    private bool _insideFaces;
    [SerializeField]
    [ConditionalHide("_shapeType", new int[] { ((int)BaseShape.Arch) })]
    private bool _outsideFaces;
    [SerializeField]
    [ConditionalHide("_shapeType", new int[] { ((int)BaseShape.Arch) })]
    private bool _frontFaces;
    [SerializeField]
    [ConditionalHide("_shapeType", new int[] { ((int)BaseShape.Arch) })]
    private bool _backFaces;
    [SerializeField]
    [ConditionalHide("_shapeType", new int[] { ((int)BaseShape.Arch) })]
    private bool _endCaps;
    [SerializeField]
    [ConditionalHide("_shapeType", new int[] { ((int)BaseShape.Cylinder) })]
    private int _smoothing = -1;
    [SerializeField]
    [ConditionalHide("_shapeType", new int[] { ((int)BaseShape.Door) })]
    private float _ledgeHeight;
    [SerializeField]
    [ConditionalHide("_shapeType", new int[] { ((int)BaseShape.Door) })]
    private float _legWidth;
    [SerializeField]
    [ConditionalHide("_shapeType", new int[] { ((int)BaseShape.Stair) })]
    private int _steps;
    [SerializeField]
    [ConditionalHide("_shapeType", new int[] { ((int)BaseShape.Stair) })]
    private bool _buildSides;



    private ProBuilderMesh _shapeMesh;
    
    public delegate void OnShapeGenerated();
    public static event OnShapeGenerated OnShapeGeneratedEvent;
    public static void RaiseOnShapeGenerated()
    {
        if (OnShapeGeneratedEvent != null)
        {
            OnShapeGeneratedEvent();                       //Invoke an Event
        }
    }

    private void OnEnable()
    {
        OnShapeGeneratedEvent += GenerateShape;
    }


    
    public void GenerateShape()
    {

        foreach (UnitController unit in _rootTransform.GetComponentsInChildren<UnitController>())
        {
            //Destroy(unit.gameObject);
            DestroyImmediate(unit.gameObject);
        }

        switch (_shapeType)
        {
            case BaseShape.Plane: CreatePlane(); break;
            case BaseShape.Cube: CreateCube(); break;
            case BaseShape.Sphere: CreateSphere(); break;
            case BaseShape.Arch: CreateArch(); break;
            case BaseShape.Cone: CreateCone(); break;
            case BaseShape.Cylinder: CreateCylinder(); break;
            case BaseShape.Door: CreateDoor(); break;
            case BaseShape.Pipe: CreatePipe(); break;
            case BaseShape.Stair: CreateStair(); break;
        }


        _shapeMesh.transform.parent = _rootTransform.parent;
        _shapeMesh.RebuildWithPositionsAndFaces(_shapeMesh.positions, _shapeMesh.faces);
        _shapeMesh.ToMesh();
        _shapeMesh.Refresh();


        RandomizeShapesGenerator.RaiseOnShapeGenerated();
       
    }

    

    private void CreatePlane() {
        _shapeMesh = ShapeGenerator.GeneratePlane(PivotLocation.Center, _width, _height, _widthCuts, _heightCuts, _axis);
    }

    private void CreateCube()
    {
        _shapeMesh = ShapeGenerator.GenerateCube(PivotLocation.Center, new Vector3(_width, _height, _depth));
    }

    private void CreateSphere()
    {
        _shapeMesh = ShapeGenerator.GenerateIcosahedron(PivotLocation.Center, _radius, _subdivision, true, false);
    }

    private void CreateArch() {
        _shapeMesh = ShapeGenerator.GenerateArch(PivotLocation.Center, _angle, _radius, _width, _depth, _radialCuts, _insideFaces, _outsideFaces, _frontFaces, _backFaces, _endCaps);
    }

    private void CreateCone()
    {
        _shapeMesh = ShapeGenerator.GenerateCone(PivotLocation.Center, _radius, _height, _subdivision);
    }


    private void CreateStair()
    {
        _shapeMesh = ShapeGenerator.GenerateStair(PivotLocation.Center, new Vector3(_width, _height, _depth), _steps, _buildSides);
    }

    private void CreatePipe()
    {
        _shapeMesh = ShapeGenerator.GeneratePipe(PivotLocation.Center, _radius, _height, _depth, _subdivision, _heightCuts);
    }

    private void CreateDoor()
    {
        _shapeMesh = ShapeGenerator.GenerateDoor(PivotLocation.Center, _width, _height, _ledgeHeight, _legWidth, _depth);
    }

    private void CreateCylinder()
    {
        _shapeMesh = ShapeGenerator.GenerateCylinder(PivotLocation.Center, _subdivision, _radius, _height, _heightCuts, _smoothing);
    }



    enum BaseShape
    {
        Plane,
        Cube,
        Sphere,
        Arch,
        Cone,
        Cylinder,
        Door,
        Pipe,
        Stair
    }



    [AttributeUsage(AttributeTargets.Field, Inherited = true)]
    private class ConditionalHideAttribute : PropertyAttribute
    {
        public string ConditionalSourceField = "";
        public List<int> IndexNumbers;

        public ConditionalHideAttribute(string conditionalSourceField, int[] indexNumbers)
        {
            this.ConditionalSourceField = conditionalSourceField;
            this.IndexNumbers = new List<int>(indexNumbers);
        }
    }


    [CustomPropertyDrawer(typeof(ConditionalHideAttribute))]
    private class ConditionalHidePropertyDrawer : PropertyDrawer
    {
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (CheckProperty(property))
            {
                EditorGUI.PropertyField(position, property, label, true);
            }
            
        }


        
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (CheckProperty(property))
            {
                return base.GetPropertyHeight(property, label); 
            }
            return 0;
        }


        private bool CheckProperty(SerializedProperty property) {
            ConditionalHideAttribute condHAtt = (ConditionalHideAttribute)attribute;
            return condHAtt.IndexNumbers.Contains(GetConditionalHideAttributeResult(condHAtt, property));
        }

        private int GetConditionalHideAttributeResult(ConditionalHideAttribute condHAtt, SerializedProperty property)
        {
            int index = -1;
            string propertyPath = property.propertyPath; //returns the property path of the property we want to apply the attribute to
            string conditionPath = propertyPath.Replace(property.name, condHAtt.ConditionalSourceField); //changes the path to the conditionalsource property path
            SerializedProperty sourcePropertyValue = property.serializedObject.FindProperty(conditionPath);

            if (sourcePropertyValue != null)
            {
                index = sourcePropertyValue.intValue;
            }
            else
            {
                Debug.LogWarning("Attempting to use a ConditionalHideAttribute but no matching SourcePropertyValue found in object: " + condHAtt.ConditionalSourceField);
            }

            return index;
        }

        
    }


}


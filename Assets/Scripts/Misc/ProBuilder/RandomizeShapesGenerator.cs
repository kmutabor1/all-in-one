using System.Collections;
using System.Collections.Generic;
using UnityEditor.ProBuilder;
using UnityEngine;
using UnityEngine.ProBuilder;
using UnityEngine.ProBuilder.MeshOperations;


public class RandomizeShapesGenerator : MonoBehaviour
{
    [SerializeField]
    private Transform _rootTransform;

    [SerializeField]
    private Material _material;
   
    
    [SerializeField]
    [Header("Extruding")]
    private ExtrudeMethod _method = ExtrudeMethod.VertexNormal;
    [SerializeField]
    [Min(10)]
    private int _trianglesMaximum = 1000;
    [SerializeField]
    private bool _isFacesDoubleSided = false;
    [SerializeField]
    [Range(-1f, 1f)]
    private float _sizeOfExtrude = 0.1f;
    [SerializeField]
    private bool _randomSize = false;
    [SerializeField]
    [Range(-1f, 0)]
    private float _randomSizeMin = -1f;
    [SerializeField]
    [Range(0, 1f)]
    private float _randomSizeMax = 1f;
    [SerializeField]
    [Min(0)]
    private int _onlyThisOrderExtrude = 1;
    [SerializeField]
    [Range(1, 5)]
    private int _repeatCount = 1;


    private ProBuilderMesh _shapeMesh;


    public delegate void OnShapeGenerated();
    public static event OnShapeGenerated OnShapeGeneratedEvent;
    public static void RaiseOnShapeGenerated()
    {
        if (OnShapeGeneratedEvent != null)
        {
            OnShapeGeneratedEvent();                       //Invoke an Event
        }
    }

    private void OnEnable()
    {
        OnShapeGeneratedEvent += GenerateNewFaces;
    }


    public void GenerateNewFaces()
    {
        _shapeMesh = _rootTransform.parent.GetComponentInChildren<ProBuilderMesh>();
        Debug.Log("Total faces in base shape: " + _shapeMesh.faces.Count + " >>> triangles: " + _shapeMesh.triangleCount);

        DoFaces();

        _shapeMesh.RebuildWithPositionsAndFaces(_shapeMesh.positions, _shapeMesh.faces);
        _shapeMesh.ToMesh();
        _shapeMesh.Refresh();

        //EditorMeshUtility.Optimize(_shapeMesh, false);

        IList<Face> faces = _shapeMesh.faces;
        IList<Vector3> positions = _shapeMesh.positions;
        Debug.Log("Total faces after face generation: " + faces.Count + " >>> triangles: " + _shapeMesh.triangleCount);

        Destroy(_shapeMesh.gameObject);

        foreach (Face face in faces)
        {
            List<Vector3> positionsV = new List<Vector3>();
            foreach (int index in face.distinctIndexes)
            {
                positionsV.Add(positions[index]);
            }
            if (_isFacesDoubleSided) {
                foreach (int index in face.distinctIndexes)
                {
                    positionsV.Add(positions[index]);
                }
            }
            
            Face[] f;
            if (face.IsQuad())
            {
                if (_isFacesDoubleSided)
                {
                    f = new Face[] {
                        new Face(new int[] { 0, 2, 1 }),
                        new Face(new int[] { 2, 3, 1 }),
                        new Face(new int[] { 5, 6, 4 }),
                        new Face(new int[] { 5, 7, 6 })
                    };
                }
                else {
                    f = new Face[] {
                        new Face(new int[] { 0, 1, 2 }),
                        new Face(new int[] { 1, 3, 2 })
                    };
                }
                
            }
            else
            {
                if (_isFacesDoubleSided)
                {
                    f = new Face[] {
                        new Face(new int[] { 0, 1, 2, 1, 3, 2 })
                    };
                }
                else
                {
                    f = new Face[] {
                        new Face(new int[] { 0, 1, 2 })
                    };
                }
               
            }

            
            ProBuilderMesh mesh = ProBuilderMesh.Create(positionsV, f);
            mesh.SetMaterial(f, _material);
            mesh.transform.parent = _rootTransform;
            mesh.RebuildWithPositionsAndFaces(mesh.positions, mesh.faces);
            mesh.ToMesh(face.IsQuad() ? MeshTopology.Quads : MeshTopology.Triangles);
            //mesh.ToMesh();

            mesh.Refresh();
            EditorMeshUtility.Optimize(mesh, false);

        }

        DestructableController.RaiseOnObjectReady();

    }


    private void DoFaces() {
        if (_onlyThisOrderExtrude > 0)
        {
            List<Face> facesToSelected = new List<Face>();
            List<List<Vector3>> alreadyExtrudedPositions = new List<List<Vector3>>();
            int trianglesCounter = _shapeMesh.triangleCount;
            for (int r = 0; r < _repeatCount; r++)
            {
                if (trianglesCounter >= _trianglesMaximum) { break; }
                Debug.Log("faceCount:" + _shapeMesh.faceCount + " triangles: " + trianglesCounter);
                
                facesToSelected.Clear();

                IList<Vector3> positions = _shapeMesh.positions;

                for (int i = 0; i < _shapeMesh.faceCount; i++)
                {
                    Face face = _shapeMesh.faces[i];

                    List<Vector3> facePositions = new List<Vector3>();
                    foreach (int index in face.distinctIndexes){
                        facePositions.Add(positions[index]);
                    }

                    
                    if (!alreadyExtrudedPositions.Exists(a => a.TrueForAll(b => facePositions.Contains(b)))) {
                        alreadyExtrudedPositions.Add(facePositions);

                        trianglesCounter += (face.distinctIndexes.Count / 3);
                        if (trianglesCounter >= _trianglesMaximum) { break; }

                        if (i % _onlyThisOrderExtrude == 0)
                        {
                            facesToSelected.Add(face);
                        }
                    }
                }
                //_shapeMesh.SetSelectedFaces(facesToSelected);

                Debug.Log("Will be extruded: " + facesToSelected.Count);
                
                if (_randomSize)
                {
                    foreach (Face face in facesToSelected)
                    {
                        ExtrudeElements.Extrude(_shapeMesh, new Face[] { face }, _method, Random.Range(_randomSizeMin, _randomSizeMax));
                    }
                }
                else
                {
                    ExtrudeElements.Extrude(_shapeMesh, facesToSelected, _method, _sizeOfExtrude);
                }
                _shapeMesh.RebuildWithPositionsAndFaces(_shapeMesh.positions, _shapeMesh.faces);
                _shapeMesh.ToMesh();
                _shapeMesh.Refresh();
                trianglesCounter = _shapeMesh.triangleCount;
            }
        }
       
    }

    enum BaseShape
    {
        Plane,
        Cube,
        Sphere,
        Arch,
        Cone,
        Cylinder,
        Door,
        Pipe,
        Stair,
        Torus,
        Sprite
    }

}

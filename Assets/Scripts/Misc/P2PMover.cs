using System;
using UnityEditor;
using UnityEngine;

public class P2PMover : MonoBehaviour
{

    [SerializeField]
    private Vector3[] _positions;

    [SerializeField]
    private MoveTypeEnum _moveType = MoveTypeEnum.Towards;
    [SerializeField]
    private float _speed = 1f;
    [SerializeField]
    [ConditionalHide("_moveType", 1)]
    private Vector3 _velocity = Vector3.one;

    private IMove _mover;
    private Vector3 targetPosition;

    private int currentPosition = 0;

    void Awake()
    {
        targetPosition = _positions[currentPosition];

        switch (_moveType)
        {
            case MoveTypeEnum.Lerp: _mover = new LerpMover(); break;
            case MoveTypeEnum.SmoothDamp: _mover = new SmoothDampMover(_velocity); break;
            case MoveTypeEnum.Towards: _mover = new TowardsMover(); break;
        }
    }


    void FixedUpdate()
    {
        if (Vector3.Distance(transform.localPosition, targetPosition) < 0.01f)
        {
            if (++currentPosition == _positions.Length) { currentPosition = 0; }

            targetPosition = _positions[currentPosition];

            if (_moveType.Equals(MoveTypeEnum.SmoothDamp))
            {
                if (!_mover.GetType().IsEquivalentTo(typeof(SmoothDampMover)))
                {
                    _mover = new SmoothDampMover(_velocity);
                }
                ((SmoothDampMover)_mover).Velocity = _velocity;
            }
        }

        transform.localPosition = _mover.DoMove(transform.localPosition, targetPosition, _speed * Time.deltaTime);
    }




    private class LerpMover : IMove
    {
        public Vector3 DoMove(Vector3 from, Vector3 to, float speed)
        {
            return Vector3.Lerp(from, to, speed);
        }
    }

    private class TowardsMover : IMove
    {
        public Vector3 DoMove(Vector3 from, Vector3 to, float speed)
        {
            return Vector3.MoveTowards(from, to, speed);
        }
    }

    private class SmoothDampMover : IMove
    {
        private Vector3 velocity;

        /// <summary>Default velocity here is Vector3.one</summary>
        public SmoothDampMover()
        {
            this.velocity = Vector3.one;
        }
        public SmoothDampMover(Vector3 velocity)
        {
            this.velocity = velocity;
        }

        public Vector3 Velocity { get => velocity; set => velocity = value; }

        public Vector3 DoMove(Vector3 from, Vector3 to, float speed)
        {
            return Vector3.SmoothDamp(from, to, ref velocity, speed);
        }
    }

 
    private interface IMove {
        public Vector3 DoMove(Vector3 from, Vector3 to, float speed);
    }

    private enum MoveTypeEnum
    {
        Lerp,
        SmoothDamp,
        Towards
    };


    [AttributeUsage(AttributeTargets.Field, Inherited = true)]
    private class ConditionalHideAttribute : PropertyAttribute
    {
        public string ConditionalSourceField = "";
        public int IndexNumber = 0;
        
        public ConditionalHideAttribute(string conditionalSourceField, int indexNumber)
        {
            this.ConditionalSourceField = conditionalSourceField;
            this.IndexNumber = indexNumber;
        }
    }

    
    [CustomPropertyDrawer(typeof(ConditionalHideAttribute))]
    private class ConditionalHidePropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            ConditionalHideAttribute condHAtt = (ConditionalHideAttribute)attribute;
            if (condHAtt.IndexNumber == GetConditionalHideAttributeResult(condHAtt, property))
            {
                EditorGUI.PropertyField(position, property, label, true);
            }
        }

        private int GetConditionalHideAttributeResult(ConditionalHideAttribute condHAtt, SerializedProperty property)
        {
            int index = -1;
            string propertyPath = property.propertyPath; //returns the property path of the property we want to apply the attribute to
            string conditionPath = propertyPath.Replace(property.name, condHAtt.ConditionalSourceField); //changes the path to the conditionalsource property path
            SerializedProperty sourcePropertyValue = property.serializedObject.FindProperty(conditionPath);

            if (sourcePropertyValue != null)
            {
                index = sourcePropertyValue.intValue;
            }
            else
            {
                Debug.LogWarning("Attempting to use a ConditionalHideAttribute but no matching SourcePropertyValue found in object: " + condHAtt.ConditionalSourceField);
            }

            return index;
        }
    }

}

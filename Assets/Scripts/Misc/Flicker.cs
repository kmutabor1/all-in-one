using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Light))]
public class Flicker : MonoBehaviour
{

    [SerializeField]
    [Min(0f)]
    private float _delayMin = 0;
    [SerializeField]
    [Min(0f)]
    private float _delayMid = 0.5f;
    [SerializeField]
    [Min(0f)]
    private float _delayMax = 1;
    [SerializeField]
    [Min(0f)]
    private float _minIntencity = 10;
    [SerializeField]
    [Min(0f)]
    private float _midIntencity = 30;
    [SerializeField]
    [Min(0f)]
    private float _maxIntencity = 200;


    private Light _light;

    private float _intervalCounter;
    private float _interval;


    private bool _isCanFlick = true;
    private bool _isInFlick = false;


    void Awake()
    {
        _light = GetComponent<Light>();
        RegenerateInterval();
    }

    void Update()
    {
        if (!_isInFlick) { // �� � �������� �������� ���� ���������� ������� �� ������
            _intervalCounter += Time.deltaTime;
            if (_isCanFlick && _intervalCounter > _interval) // ������������ ���������� � ��������
            {
                _isInFlick = true;
            }
        } else {
            if (_isCanFlick) // ������ �������� ����������� ����� �������� ��������
            {
                _isCanFlick = false; // �������������� ��������� �������� ��������
                RegenerateInterval(); // ����� ���������, ����� ������ ��������� �� ������ ���������� ��������
                StartCoroutine(DoFlick()); // ������ ������
            }
        }
    }


    private IEnumerator DoFlick() {
        _light.intensity = _minIntencity;
        yield return new WaitForSeconds(Random.Range(0, _delayMin));

        _light.intensity = _midIntencity;
        yield return new WaitForSeconds(Random.Range(0, _delayMid));

        _light.intensity = _maxIntencity;
        yield return new WaitForSeconds(Random.Range(0, _delayMax));

        _isCanFlick = true; // ������ ������ � ����������� ��������� ����� ��������
        _isInFlick = false; // ����� �� ��������. ���������� ������� ����� �����������
        yield return new WaitForSeconds(_interval);
        
    }

    
    
    private void RegenerateInterval() {
        _interval = Random.Range(Mathf.Min(_delayMin, _delayMid, _delayMax), Mathf.Max(_delayMin, _delayMid, _delayMax));
        _intervalCounter = 0;
        //Debug.Log("I: " + _interval);
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnviromentController : MonoBehaviour
{

    [SerializeField]
    private Transform _envTransform;

    [SerializeField]
    private float _envRotationSpeed = 0.5f;

    void FixedUpdate()
    {
        _envTransform.Rotate(0, _envRotationSpeed * Time.fixedDeltaTime, 0);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

//[RequireComponent(typeof(CharacterController))]
public class CameraMovement : MonoBehaviour
{

    [SerializeField]
    private float _moveSpeed = 10;
    [SerializeField]
    private float _lookSpeed = 10;
    
    
    private Vector2 _move;
    private Vector2 _moveVertical;
    private Vector2 _look;

   
    void Awake()
    {
        Controls _controls = ControlsC.getControls();

        //Cursor.lockState = CursorLockMode.Locked;
        _controls.Camera.Move.performed += ctx => _move = ctx.ReadValue<Vector2>();
        _controls.Camera.Move.canceled += ctx => _move = Vector2.zero;
        
        _controls.Camera.MoveVertical.performed += ctx => _moveVertical = ctx.ReadValue<Vector2>();
        _controls.Camera.MoveVertical.canceled += ctx => _moveVertical = Vector2.zero;
        
        _controls.Camera.Look.performed += ctx => _look = ctx.ReadValue<Vector2>();
        _controls.Camera.Look.canceled += ctx => _look = Vector2.zero;

        _controls.Camera.Filter.performed += ctx => F();
    }


    private void Update()
    {
        transform.Translate(new Vector3(_move.x, _moveVertical.y, _move.y) * Time.deltaTime * _moveSpeed, Space.Self);
        transform.localEulerAngles += new Vector3(-_look.y, _look.x, 0) * Time.deltaTime * _lookSpeed;
    }


    private void F()
    {
        //Maze.RaiseOnRegenerate();
        //Generator.RaiseOnShapeGenerated();
        
        GravityController.RaiseOnGavity();
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructableController : MonoBehaviour
{
    [SerializeField]
    private Transform _rootTransform;

    [SerializeField]
    private float _restoreSpeed = 10f;
    [SerializeField]
    private float _scaleUpSpeed = 1f;
    [SerializeField]
    [Min(2)]
    private float _explosionMaxDistance = 25f;
    [SerializeField]
    private float _forceSpeed = 1f; 
    [SerializeField]
    private float _torqueSpeed = 0.3f;
    [SerializeField]
    private bool _shrinkToPoint = false;
    [SerializeField]
    private bool _scaleToZeroSize = false;


    public delegate void OnObjectReady();
    public static event OnObjectReady OnObjectReadyEvent;
    public static void RaiseOnObjectReady()
    {
        if (OnObjectReadyEvent != null)
        {
            OnObjectReadyEvent();                       
        }
    }

    public delegate void OnObjectExplosion();
    public static event OnObjectExplosion OnObjectExplosionEvent;
    public static void RaiseOnObjectExplosion()
    {
        if (OnObjectExplosionEvent != null)
        {
            OnObjectExplosionEvent();
        }
    }

    private void Awake()
    {
        Controls _controls = ControlsC.getControls();
        _controls.Camera.Filter.performed += ctx => CreateAndDestroyObject();
    }


    private void OnEnable()
    {
        OnObjectReadyEvent += ObjectReady;
        OnObjectExplosionEvent += Explosion;
    }


    private void CreateAndDestroyObject() {
        Generator.RaiseOnShapeGenerated();
    }

    private void ObjectReady() {
        ReadyUpObject(_rootTransform);
        Explosion();
    }

    
    private void Explosion()
    {
        UnitController.RaiseOnExplosion();
    }

    private void ReadyUpObject(Transform root) {
        for(int i = 0; i< root.childCount; i++) {
            Transform unit = root.GetChild(i);

            if (unit.GetComponent<Renderer>()) {
                AddElementEndTransform(unit, unit.localPosition, unit.localRotation, unit.lossyScale);
            }

            ReadyUpObject(unit);

            if(_shrinkToPoint) {
                unit.position = _rootTransform.localPosition;
            }

            if (_scaleToZeroSize) {
                unit.localScale = Vector3.zero;
            }
        }
    }


    private void AddElementEndTransform(Transform targetObject, Vector3 position, Quaternion rotation, Vector3 scale)
    {
        UnitController unit = targetObject.gameObject.AddComponent<UnitController>();
        unit.SetTargets(position, rotation, scale, _restoreSpeed, _scaleUpSpeed, _explosionMaxDistance, _forceSpeed, _torqueSpeed);
    }

}

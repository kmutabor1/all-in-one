using UnityEngine;


public class ControlsC : MonoBehaviour
{

    private static Controls _controls;

    void Awake()
    {
        _controls = new Controls();
        
    }

    public static Controls getControls()
    {
        return _controls;
    }


    private void OnEnable()
    {
        _controls.Enable();
    }
    private void OnDisable()
    {
        _controls.Disable();
    }
}

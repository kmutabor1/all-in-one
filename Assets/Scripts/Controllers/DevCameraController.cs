using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class DevCameraController : MonoBehaviour
{

    [SerializeField]
    [Range(0.1f, 1.5f)]
    private float _speed = 1f;
    [SerializeField]
    [Range(0.1f, 2f)]
    private float _sens = 1f;


    private CharacterController _cc;
    private float yaw, pitch = 0f;

    // Start is called before the first frame update
    void Start()
    {
        _cc = GetComponent<CharacterController>();
        yaw = transform.eulerAngles.y;
        pitch = transform.eulerAngles.x; 

    }

    // Update is called once per frame
    void Update()
    {
        
        yaw += Input.GetAxis("Mouse X") * _sens;
        pitch -= Input.GetAxis("Mouse Y") * _sens;

        transform.eulerAngles = new Vector3(pitch, yaw, 0f);

        if (Input.GetKey(KeyCode.Q)) {
            _cc.Move(-transform.up * _speed);
        }
        if (Input.GetKey(KeyCode.E)) {
            _cc.Move(transform.up * _speed);
        }

        _cc.Move((transform.right * Input.GetAxis("Horizontal") + transform.forward * Input.GetAxis("Vertical")) * _speed);

    }
}
